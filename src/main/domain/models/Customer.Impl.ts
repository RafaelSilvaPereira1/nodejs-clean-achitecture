import {CustomerInterface} from "../user_cases/Customer.Interface";

/**
 * this class is a implementation for the CustomerInterface
 */
export class CustomerImpl implements CustomerInterface {

  private readonly cpf: string;
  private _name: string;
  private _email: string;
  private _address: string;

  /**
   * build a Customer from your properties:
   * @param cpf
   * @param name
   * @param email
   * @param address
   */
  constructor(cpf: string, name: string, email: string, address: string) {
    this.cpf = cpf;
    this._name = name;
    this._email = email;
    this._address = address;
  }


  /**
   *
   * @param other
   * @returns true if this other Customer have the id equals for this Customer id
   */
  equals(other: CustomerInterface): boolean {
    return this.getId() === other.getId();
  }

  /**
   * @return a cpf from the current Customer
   */
  getId(): string {
    return this.cpf;
  }

  /**
   * @returns a representation for this class
   */
  toString(): string {
    return JSON.stringify(this);
  }


  // GETTERS AND SETTERS
  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get address(): string {
    return this._address;
  }

  set address(value: string) {
    this._address = value;
  }
}
