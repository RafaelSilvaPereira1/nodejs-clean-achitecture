import {CustomerFactoryInterface} from "../user_cases/Customer.Factory.Interface";
import {CustomerInterface} from "../user_cases/Customer.Interface";
import {CustomerImpl} from "./Customer.Impl";

export class CustomerFactoryImpl implements CustomerFactoryInterface {

  /**
   * create a Customer for your properties
   * @param cpf
   * @param name
   * @param email
   * @param address
   */
  create(cpf: string, name: string, email: string, address: string): CustomerInterface {
    return new CustomerImpl(cpf, name, email, address);
  }

  /**
   * edit a specific Customer
   * @param customer
   * @param name
   * @param email
   * @param address
   */
  edit(customer: CustomerImpl, name: string, email: string, address: string): CustomerInterface {
    if(!!name) customer.name = name;
    if(!!email) customer.email = email;
    if(!!address) customer.address = address;
    return customer;
  }

}
