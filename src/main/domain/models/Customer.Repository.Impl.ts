import {CustomerRepositoryInterface} from "../user_cases/Customer.Repository.Interface";
import {CustomerInterface} from "../user_cases/Customer.Interface";


/**
 * Essa classe so faria sentido se eu tivesse usado algum banco de dados, mas de forma generica ele possibilita
 * as funções basicas do crud
 */
export class CustomerRepositoryImpl implements CustomerRepositoryInterface {
  private genericDB: any; //  a ideia aqui era ter um banco de dados como mongo ou mysql ou postgres ...

  constructor(db: any) {
    this.genericDB = db
  }

  /**
   * delete a specific customer register from database
   * @param cpf
   */
  delete(cpf: string): number {
    return this.genericDB.delete(cpf);
  }

  /**
   * get a specific customer register from database
   * @param cpf
   */
  get(cpf: string): CustomerInterface {
    return this.genericDB.get(cpf);
  }

  /**
   * read all register form database
   */
  readAll(): Set<CustomerInterface> {
    return this.genericDB.read();
  }

  /**
   * alter a specific customer register
   * @param cpf
   * @param editedCustomer
   */
  edit(cpf: string, editedCustomer: CustomerInterface): number {
    try{
      this.genericDB.alterRegister(cpf, editedCustomer);
      return 200; // OK
    } catch (e) {
      return 788481// algum codigo de erro de banco de dados
    }

  }

  /**
   * save a customer in a register on database
   * @param customer
   */
  save(customer: CustomerInterface): number {
    try {
      this.genericDB.save(customer);
      return 200; // OK
    }catch (e) {
      return  4948 // codigo de erro
    }
  }

}
