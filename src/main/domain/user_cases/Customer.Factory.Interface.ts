import {CustomerInterface} from "./Customer.Interface";

/**
 * describe a generic interface able to handle create and edit a customer
 */
export interface CustomerFactoryInterface {

  /**
   * create a new customer from properties
   * @param cpf
   * @param name
   * @param email
   * @param address
   */
  create(cpf: string, name: string, email: string, address: string): CustomerInterface;

  /**
   * edit a existing customer
   * @param customer
   * @param name
   * @param email
   * @param address
   */
  edit(customer: CustomerInterface, name: string, email: string, address: string): CustomerInterface;
}
