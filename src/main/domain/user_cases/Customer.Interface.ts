/**
 * describe a default customer
 */
export interface CustomerInterface {

  toString(): string;

  equals(other: CustomerInterface): boolean;

  getId(): string;
}
