import {CustomerInterface} from "./Customer.Interface";

/**
 * describe a interface able to handle a database
 */
export interface CustomerRepositoryInterface {

  /**
   * read all customers register from database
   */
  readAll(): Set<CustomerInterface>;

  /**
   * read a specific customer register
   * @param cpf
   */
  get(cpf: string): CustomerInterface;

  /**
   * save a Customer in database
   * @param customer
   */
  save(customer: CustomerInterface): number;

  /**
   * edit a specific customer in database
   * @param cpf
   * @param editedCustomer
   */
  edit(cpf: string, editedCustomer: CustomerInterface): number;

  /**
   * delete a specific customer in database
   * @param cpf
   */
  delete(cpf: string): number;
}
