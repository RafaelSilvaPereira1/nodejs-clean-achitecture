import { CustomerRepositoryInterface } from "../domain/user_cases/Customer.Repository.Interface";
import { CustomerFactoryInterface } from "../domain/user_cases/Customer.Factory.Interface";
import { CustomerInterface } from "../domain/user_cases/Customer.Interface";

export class CustomerController {
  private customerRepository: CustomerRepositoryInterface;
  private customerFactory: CustomerFactoryInterface;

  /**
   * build a controller to handle with customer functions
   * @param customerRepository: responsible for dealing with the persistence of the consumer object
   * @param customerFactory: responsible for dealing with creation of de customer object
   */
  constructor(customerRepository: CustomerRepositoryInterface, customerFactory: CustomerFactoryInterface) {
    this.customerRepository = customerRepository;
    this.customerFactory = customerFactory;
  }

  /**
   * @returns a set of all register of Customers in database
   */
  readAll(): Set<CustomerInterface> {
    return this.customerRepository.readAll();
  }

  /**
   * read specific customer register
   * @param cpf: a id for the get the specific customer register
   */
  get(cpf: string): CustomerInterface {
    return this.customerRepository.get(cpf);
  }

  /**
   * create a customer register in database, from your properties:
   * @param cpf
   * @param name
   * @param email
   * @param address
   */
  create(cpf: string, name: string, email: string, address: string): number {
    const customer: CustomerInterface = this.customerFactory.create(cpf, name, email, address);
    return this.customerRepository.save(customer);
  }

  /**
   * edit a specific customer register:
   * @param id: required
   * @param name: optional
   * @param email: optional
   * @param address: optional
   */
  edit(id: string, name: string, email: string, address: string): number {
    const customer: CustomerInterface = this.customerRepository.get(id);
    const editedCustomer: CustomerInterface = this.customerFactory.edit(customer, name, email, address);
    return this.customerRepository.edit(id, editedCustomer);
  }

  /**
   * delete a specific customer from database
   * @param cpf: customer id
   */
  delete(cpf: string): number {
    return this.customerRepository.delete(cpf);
  }
}
