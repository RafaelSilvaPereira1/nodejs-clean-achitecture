import {CustomerController} from "../controllers/Customer.Controller";


/**
 * This class defines a adapter from CustomerController (controller level) to (infra level)
 */
export class CustomerControllerAdapter {


  private customerController: CustomerController;

  /**
   * get all register of customer's of database
   */
  readAll(): string {
    return JSON.stringify(this.customerController.readAll());
  }

  /**
   * get the customer register corresponding with id
   * @param id: id of customer (your cpf)
   */
  get(id: string): string {
    return JSON.stringify(this.customerController.get(id));
  }

  /**
   * delete the customer register corresponding with id
   * @param id: id of customer (your cpf)
   */
  delete(id: string): string {
    return JSON.stringify(this.customerController.delete(id));
  }

  /**
   * edit the customer register corresponding with id
   * @param id: id of customer (your cpf)
   */
  edit(id: string, name: string, email: string, addres: string): string {
    return JSON.stringify(this.customerController.edit(id, name, email, addres));
  }


  /**
   * create a Customer register from your attributes
   */
  create(cpf: string, name: string, email: string, address: string): string {
    return JSON.stringify(this.create(cpf, name, email, address));
  }
}
