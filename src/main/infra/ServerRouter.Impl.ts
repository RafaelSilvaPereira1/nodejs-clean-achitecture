import {ServerRouterInterface} from './ServerRouter.Interface';
import {Application} from 'express';
import {CustomerControllerAdapter} from "../adapters/Customer.Controller.Adapter";
import {CustomerRouterFunctionsBearer} from "./Customer.RouterFunctions.Bearer";




/**
 *
 * @class ServerRouterImpl is a implementation of ServerRouterInterface
 */
export class ServerRouterImpl implements ServerRouterInterface {
  private customerRouter: CustomerRouterFunctionsBearer;

  constructor(customerRouter: CustomerRouterFunctionsBearer) {
    this.customerRouter = customerRouter;
  }

  /**
   * Register all server routes
   * @param app: a instance of express.Application
   */
  setRoutes(app: Application): void {
    app.get('/', (req, res) => res.send('Hello World'));
    this.setCustomerRoutes(app);
  }

  /**
   * Register server routes to entity Controller
   * @param app: a instance of express.Application
   */
  private setCustomerRoutes(app: Application): void {
    app.get('/customer', this.customerRouter.getAllCustomers);
    app.get('/customer/:id', this.customerRouter.getCustomer);
    app.delete('/customer/:id', this.customerRouter.deleteCustomer);
    app.put('/customer/:id', this.customerRouter.editCustomer);
    app.post('/customer', this.customerRouter.create)
  }
}
