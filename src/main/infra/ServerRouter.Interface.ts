import {Application} from 'express';

/**
 * This interface define a main method for define all endpoint's of server
 * @author: Rafael da Silva Pereira
 */
export interface ServerRouterInterface {

  /**
   * Method define server endpoint's
   * @param app
   */
  setRoutes(app: Application): void;

}
