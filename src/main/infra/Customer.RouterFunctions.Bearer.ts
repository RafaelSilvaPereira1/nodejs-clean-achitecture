import {CustomerControllerAdapter} from "../adapters/Customer.Controller.Adapter";


/**
 * this class is responsible for provides to server router functions to customer handler
 */
export class CustomerRouterFunctionsBearer {

  private customerController: CustomerControllerAdapter;

  /**
   * get all register of customer's of database
   * @param req: user request
   * @param res: server response
   */
  public getAllCustomers(req, res) : string{
    return res.send(this.customerController.readAll());
  }

  /**
   * get the customer register
   * @param req: user request
   * @param res: server response
   */
  public getCustomer(req, res) : string{
    return this.functionToOneCustomer(req, res, this.customerController.get);
  }

  /**
   * delete the customer register
   * @param req: user request
   * @param res: server response
   */
  public deleteCustomer(req, res) : string {
    return this.functionToOneCustomer(req, res, this.customerController.delete);
  }

  /**
   * edit the customer register
   * @param req: user request
   * @param res: server response
   */
  public editCustomer(req, res): string{
    const id: string = req.params.id;
    const body = req.body;
    if (!!id) {
      const name: string = body['name'];
      const email: string = body['email'];
      const address: string = body['address'];
      return res.send(this.customerController.edit(id, name, email, address))
    } else {
      return res.send("Invalid id").status(400);
    }
  }

  /**
   * create the customer register
   * @param req: user request
   * @param res: server response
   */
  public create(req, res): string {
    const body = req.body;
    const cpf: string = body['cpf'];
    const name: string = body['name'];
    const email: string = body['email'];
    const address: string = body['address'];

    return res.send(this.customerController.create(cpf, name, email, address));
  }

  /**
   * execute a function if the customer id is send in request
   * @param req: client request
   * @param res: server response
   * @param callback: server function
   */
  private functionToOneCustomer(req, res, callback): string{
    const id: string = req.params.id;
    if (!!id) {
      return res.send(callback(id));
    } else {
      return res.send("Invalid id").status(400);
    }
  }



}
