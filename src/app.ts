import express from 'express';
import cors from 'cors';
import {ServerRouterInterface} from "./main/infra/ServerRouter.Interface";
import {ServerRouterImpl} from "./main/infra/ServerRouter.Impl";


// eslint-disable-next-line require-jsdoc,no-unused-vars
class App {
  public express: express.Application;
  private router: ServerRouterInterface;

  /**
   * build a server app
   * @param router
   */
  constructor(router: ServerRouterInterface) {
    this.express = express();
    this.initMiddlewers();
    this.router = router;
    this.router.setRoutes(this.express);
  }

  /**
   *
   */
  private initMiddlewers(): void {
    this.express.use(express.json());
    this.express.use(cors());
  }
}


const router: ServerRouterInterface = new ServerRouterImpl();

export default new App(router).express;
